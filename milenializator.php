<?php

$texto = "qué que porque sí buenas bueno capullo ¡¡¡hola!!! ¿hola? igual";

function milenializator($elem){
    $replace = array( 
        '¡' => '',
        '¿' => '',
        'Á' => 'A',
        'É' => 'E',
        'Í' => 'I',
        'Ó' => 'O',
        'Ú' => 'U',
        'á' => 'a',
        'é' => 'e',
        'í' => 'i',
        'ó' => 'o',
        'ú' => 'u',
        'que' => 'k', 
        'porque' => 'xq',
        'gu' => 'w',
        'bu' => 'w',
        'igual' => '=',
        'c' => 'k'
    );
    for ($i=0, $c=strlen($elem); $i<$c; $i++){
        $elem[$i] = (rand(0, 100) > 50 ? strtoupper($elem[$i]) : strtolower($elem[$i]));
    }
    return (strtr($elem, $replace));
}

echo milenializator($texto);