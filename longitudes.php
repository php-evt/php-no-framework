<?php
$ejemplo = ["abcd","abc","de","hjjj","g","wer"];

$max = 0;
$min = 0;

foreach($ejemplo as $cadena){
    $long = strlen($cadena);

    if($min == 0 || $long < $min){
        $min = $long;
    }

    if($long > $max){
        $max = $long;
    }
}

echo "la longitud máxima es $max y la mínima $min";