<?php

$numeros = [12,13,2,4,5,4];

function sumaNumeros($num){
    $total = 0;
    foreach($num as $numero){
        $total += $numero;
    }
    return $total;
}

echo "La suma del array es: ";
print_r(sumaNumeros($numeros));