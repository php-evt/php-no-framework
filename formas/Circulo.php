<?php

require 'Poligono.php';

class Circulo extends Poligono{
    private $radio;

    public function __construct($radio){
        $this->radio = $radio;
    }

    public function calcularArea(){
        echo "El área del cuadrado es : " . M_PI * $this->radio**2;
    }
}

$circulo = new Circulo(5);

$circulo->calcularArea();