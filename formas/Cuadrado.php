<?php

require 'Poligono.php';

class Cuadrado extends Poligono{

    private $lado;

    public function __construct($lado){
        $this->lado = $lado;
    }

    function calcularArea(){
        echo "El área del cuadrado es : " . $this->lado * $this->lado;
    }
}

$cuadrado = new Cuadrado(5);

$cuadrado->calcularArea();