<?php

require 'Poligono.php';

class Rectangulo extends Poligono{

    private $base;
    private $altura;

    public function __construct($base, $altura){
        $this->base = $base;
        $this->altura = $altura;
    }

    public function calcularArea(){
        echo "El área del rectángulo es : " . $this->base * $this->altura;
    }
}

$rect = new Rectangulo(3,5);

$rect->calcularArea();