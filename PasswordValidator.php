<?php

function evaluar_array($texto, $needle) {
    foreach($needle as $caracter) {
        if(($pos = strpos($texto, $caracter))!==false){
            return $pos;
        }
    }
    return false;
}

class PasswordValidator{

    const LONGITUD = 8;

    function esValida($pass){
        $cadena_buscada = array('.',',','-','_',';');
        
        $hay_caracter = evaluar_array($pass, $cadena_buscada);
        if(strlen($pass) >= self::LONGITUD){
            if ($hay_caracter === false) {
                echo "Debe contener un caracter especial";
            } else {
                echo "Contraseña válida";
            }
        }else{
            echo "Contraseña no válida";
        }
    }
}

$text = new PasswordValidator;

$text->esValida('eduardo-vidal');