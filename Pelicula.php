<?php

class Pelicula{
    var $titulo;
    var $sinopsis;
    var $anyo;

    function __construct($titulo, $sinopsis, $anyo){
        $this->titulo = $titulo;
        $this->sinopsis = $sinopsis;
        $this->anyo = $anyo;
    }
}