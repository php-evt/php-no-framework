<?php

namespace Formas;

abstract class Poligono{
    public abstract function calcularArea();
}