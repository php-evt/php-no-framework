<?php

namespace Formas\formas;
use Formas\Poligono;

class Cuadrado extends Poligono{

    private $lado;

    public function __construct($lado){
        $this->lado = $lado;
    }

    function calcularArea(){
        echo "El área del cuadrado es : " . $this->lado * $this->lado;
    }
}