<?php
namespace Formas\formas;
use Formas\Poligono;

class Circulo extends Poligono{
    private $radio;

    public function __construct($radio){
        $this->radio = $radio;
    }

    public function calcularArea(){
        echo "El área del círculo es : " . M_PI * $this->radio**2;
    }
}

