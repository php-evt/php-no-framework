<?php
require_once ("vendor/autoload.php");

use Formas\Poligono as Poli;
use Formas\formas\Cuadrado as Cuadrado;
use Formas\formas\Circulo as Circulo;
use Formas\formas\Rectangulo as Rectangulo;

$circulo = new Circulo(5);

$circulo->calcularArea();

$cuadrado = new Cuadrado(5);

$cuadrado->calcularArea();


$rect = new Rectangulo(3,5);

$rect->calcularArea();