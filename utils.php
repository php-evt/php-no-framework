<?php

function sumaNumeros($num){
    $total = 0;
    foreach($num as $numero){
        $total += $numero;
    }
    return $total;
}

function milenializator($texto){
	$original_text = array('que', 'porque', 'gu', 'bu', 'igual', '¿', '¡','á', 'é', 'í', 'ó', 'ú');
    $milenial_text = array('k', 'xq', 'w', 'w', '=', '', '', 'a', 'e', 'i', 'o', 'u');
	for ($i=0, $c = strlen($texto); $i<$c ; $i++){
		$texto[$i] = (rand(0, 100) > 50 ? strtoupper($texto[$i]) : ($texto[$i]));
	}
	return(str_ireplace($original_text, $milenial_text, $texto));
}

